export default class PetHelpers {
  static convertAgeToDob(age: number | string): Date | undefined {
    // since zero is a valid pet age, we need to get more granular
    if (typeof age === 'string') {
      if (!age.length) {
        return undefined
      } else {
        age = +age
      }
    }
    const today = new Date()
    const dob = new Date(today.getFullYear(), today.getMonth(), today.getDate())
    // just a lil puppy, 42 days old
    if (age < 1) {
      dob.setDate(today.getDate() - 42)
    } else {
      dob.setFullYear(today.getFullYear() - age)
    }
    return dob
  }
}
