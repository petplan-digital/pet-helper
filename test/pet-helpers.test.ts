import PetHelpers from '../src/pet-helpers'

/**
 * Dummy test
 */

const today = new Date()
const dob = new Date(today.getFullYear(), today.getMonth(), today.getDate())

describe('Pet Helpers', () => {
  it('PetHelpers is instantiable', () => {
    expect(new PetHelpers()).toBeInstanceOf(PetHelpers)
  })

  it('Should Convert an age to a date', () => {
    let test = PetHelpers.convertAgeToDob(2)
    const dob = new Date(today.getFullYear(), today.getMonth(), today.getDate())

    dob.setFullYear(today.getFullYear() - 2)
    expect(test).toEqual(dob)
  })

  it('Should work with strings', () => {
    let string = '3'
    let test = PetHelpers.convertAgeToDob(string)
    const dob = new Date(today.getFullYear(), today.getMonth(), today.getDate())

    dob.setFullYear(today.getFullYear() - 3)
    expect(test).toEqual(dob)
  })

  it('Should be undefined with empty string', () => {
    let string = ''
    let test = PetHelpers.convertAgeToDob(string)
    expect(test).toBe(undefined)
  })

  it('Should handles dogs who are less then 1', () => {
    let newPuppy = PetHelpers.convertAgeToDob(0)
    const dob = new Date(today.getFullYear(), today.getMonth(), today.getDate())

    dob.setDate(today.getDate() - 42)
    expect(newPuppy).toEqual(dob)
  })
})
